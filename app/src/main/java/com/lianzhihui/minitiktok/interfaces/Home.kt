package com.lianzhihui.minitiktok.interfaces

import android.util.Log
import com.by.net.ApiTool
import com.echofeng.common.MyApplication
import com.echofeng.common.config.AppConfig
import com.echofeng.common.config.DataManager
import com.echofeng.common.net.APIConstant
import com.echofeng.common.utils.AESCBCCrypt2
import com.echofeng.common.utils.SystemUtils
import com.mail.comm.net.ApiListener
import org.json.JSONObject
import org.xutils.common.util.KeyValue
import org.xutils.common.util.LogUtil
import org.xutils.http.RequestParams
import java.io.File

class Home {


    //文件上传
    fun a(path: String, apiListener: ApiListener) {

        val timeStamp: String = (System.currentTimeMillis() / 1000).toString()
        val singPostBody = DataManager.singFileBody(timeStamp + "")
        var url = AppConfig.upload_url + "/upload?" + "platform=A&timestamp=" + timeStamp + "&dir=user&sign=" + singPostBody
        val params = RequestParams(url)
        params.isMultipart = true
        params.addBodyParameter("file", File(path), "multipart/form-data")
        ApiTool().postApi(params, apiListener, "file/upload")
    }

    fun a1(title: String, cover: String, apiListener: ApiListener) {

        val params = RequestParams(APIConstant.getBASE_URL() + "/app/api/album/create")
        params.addBodyParameter("title", title)
        params.addBodyParameter("cover", cover)
        ApiTool().postApi2(params, apiListener, "create/cooper")

    }

    fun a2(module: String, page: Int, apiListener: ApiListener) {
        val params = RequestParams(APIConstant.getBASE_URL() + "/app/api/creator/video/lists")
        params.addBodyParameter("module", module)
        params.addBodyParameter("page", page.toString())
        params.addBodyParameter("page_size", "15")
        ApiTool().postApi2(params, apiListener, "cooper/lists")
    }

}