package com.lianzhihui.minitiktok.base

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.RelativeLayout
import com.mail.comm.net.ApiListener
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.Callback
import org.xutils.http.RequestParams
import java.util.ArrayList

abstract class BaseAty:AutoLayoutActivity(), ApiListener {

    var fragments: ArrayList<BaseFrg>? = null

    var currentFragment: BaseFrg? = null

    open fun getFragmentContainerId(): Int = 0

    open fun getFragmentContainerId2(): Int = 0

    abstract fun getLayoutId(): Int

    var loadingDialog: XLoadDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        loadingDialog?.destroy()

    }

    override fun onDestroy() {
        super.onDestroy()
        loadingDialog?.destroy()
    }


    fun startProgressDialog() {
        if (loadingDialog == null) loadingDialog =
            XLoadDialog()
        loadingDialog?.showDialogForLoading(this)
    }

    fun stopProgressDialog() = loadingDialog?.cancelDialogForLoading()


    fun startActivity(cls: Class<*>) {
        startActivity(cls, null)
    }


    fun startActivity(cls: Class<*>, bundle: Bundle?) {
        val intent = Intent(this, cls)
        bundle?.let { intent.putExtras(it) }
        startActivity(intent)
//        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun startActivityForResult(cls: Class<*>, requestCode: Int) {
        startActivityForResult(
            cls,
            null,
            requestCode
        )
    }

    fun startActivityForResult(cls: Class<*>, bundle: Bundle?, requestCode: Int) {
        val intent = Intent(this, cls)
        bundle?.let { intent.putExtras(it) }
        startActivityForResult(intent, requestCode)
//        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun addFragment(cls: Class<*>, data: Any?) {
        val param = FragmentParam()
        param.cls = cls
        param.data = data
        param.addToBackStack = false
        processFragement(param)
    }

    fun initTopview(relay: RelativeLayout?, bgColor: String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(150)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

    override fun onCancelled(var1: Callback.CancelledException?) {}

    override fun onComplete(var1: RequestParams?, var2: String?, type: String?) {
    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }

    override fun onExceptionType(var1: Throwable?, params: RequestParams?, type: String?) {
    }



    private fun getFragmentTag(param: FragmentParam): String = StringBuilder(param.cls.toString() + param.tag).toString()

    private fun addDistinctEntry(sourceList: ArrayList<BaseFrg>?, entry: BaseFrg): Boolean {
        return sourceList != null && !sourceList.contains(entry) && sourceList.add(entry)
    }

    private fun processFragement(param: FragmentParam) {
        val containerId: Int = if (!TextUtils.isEmpty(param.Id)) getFragmentContainerId2() else getFragmentContainerId()
        val cls = param.cls
//        if (cls == null) return
        try {
            val e = getFragmentTag(param)
            Log.e("processFragement tag=",e)
            var f1 = supportFragmentManager.findFragmentByTag(e)
            var fragment = f1?.let { it as BaseFrg } ?: cls.newInstance() as BaseFrg

            if (this.fragments == null) this.fragments = ArrayList()
            addDistinctEntry(this.fragments, fragment)
            val ft = supportFragmentManager.beginTransaction()

            if (param.type != FragmentParam.TYPE.ADD)
                ft.replace(containerId, fragment, e)
            else if (!fragment.isAdded) {
                if (this.currentFragment != null) this.currentFragment!!.onPause()
                ft.add(containerId, fragment, e)
                if (param.data != null) fragment.initRefreshData(param.data)
            } else {
                val var7 = this.fragments!!.iterator()
                while (var7.hasNext()) ft.hide(var7.next())
                if (this.currentFragment != null) this.currentFragment!!.onPause()
                ft.show(fragment)
                fragment.onResume()
                if (param.data != null) fragment.refreshData(param.data)
            }
            this.currentFragment = fragment
            if (param.addToBackStack) ft.addToBackStack(e)
            ft.commitAllowingStateLoss()
        } catch (var9: InstantiationException) {
            var9.printStackTrace()
        } catch (var10: IllegalAccessException) {
            var10.printStackTrace()
        }
    }
}