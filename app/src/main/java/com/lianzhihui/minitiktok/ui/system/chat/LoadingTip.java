package com.lianzhihui.minitiktok.ui.system.chat;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lianzhihui.onlyleague.R;


public class LoadingTip extends LinearLayout {

    private ImageView img_tip_logo;
    private ImageView imgv_loading;
    private ImageView imgv_refresh;

    private ProgressBar progress;
    private TextView tv_tips;
    private TextView bt_operate;
    private TextView tv_lar;
    private TextView tv_refresh;
    private String errorMsg;
    private onReloadListener onReloadListener;
    private onReloadImgListener onReloadImgListener;
    private onLarListener onLarListener;

    private RelativeLayout relay_bg;


    public LoadingTip(Context context) {
        super(context);
        initView(context);
    }

    public LoadingTip(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public LoadingTip(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoadingTip(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }


    public enum LoadStatus {
        sereverError, error, empty, loading, loading2, loading3,finish, lar, empty2, refresh,refresh_white
    }

    private void initView(Context context) {
        View.inflate(context, R.layout.dialog_loading_tip, this);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        img_tip_logo = (ImageView) findViewById(R.id.img_tip_logo);
        progress = (ProgressBar) findViewById(R.id.progress);
        tv_tips = (TextView) findViewById(R.id.tv_tips);
        tv_lar = (TextView) findViewById(R.id.tv_lar);
        bt_operate = (TextView) findViewById(R.id.bt_operate);
        tv_refresh = (TextView) findViewById(R.id.tv_refresh);
        imgv_loading = (ImageView) findViewById(R.id.imgv_loading);
        imgv_refresh = (ImageView) findViewById(R.id.imgv_refresh);
        relay_bg = (RelativeLayout) findViewById(R.id.relay_bg);


        bt_operate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadListener != null) {
                    onReloadListener.reload();
                }
            }
        });
        imgv_refresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadImgListener != null) {
                    onReloadImgListener.reloadImg();
                }
            }
        });
        tv_refresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadImgListener != null) {
                    onReloadImgListener.reloadImg();
                }
            }
        });
        //登陆
        tv_lar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onLarListener != null) {
                    onLarListener.lar();
                }
            }
        });
        setVisibility(View.GONE);
    }

    public void setTips(String tips) {
        if (tv_tips != null) {
            tv_tips.setText(tips);
        }
    }


    public void setLoadingTip(LoadStatus loadStatus) {
        switch (loadStatus) {
            case empty:
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tv_tips.setText(getContext().getText(R.string.empty).toString());
                img_tip_logo.setImageResource(R.drawable.empty_nodata);
                bt_operate.setVisibility(View.VISIBLE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                tv_lar.setText("暂无数据哟~");
                imgv_loading.setVisibility(GONE);
                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case empty2:
                relay_bg.setBackgroundColor(0xffeeeeee);
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                tv_tips.setText(getContext().getText(R.string.empty).toString());
                img_tip_logo.setImageResource(R.drawable.empty_nodata);
                bt_operate.setVisibility(View.VISIBLE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                tv_lar.setText("暂无数据哟~");
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case sereverError:
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                if (TextUtils.isEmpty(errorMsg)) {
                    tv_tips.setText(getContext().getText(R.string.net_error).toString());
                } else {
                    tv_tips.setText(errorMsg);
                }
                img_tip_logo.setImageResource(R.drawable.empty_no_network);
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case error:
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);
                if (TextUtils.isEmpty(errorMsg)) {
                    tv_tips.setText(getContext().getText(R.string.net_error).toString());
                } else {
                    tv_tips.setText(errorMsg);
                }
                img_tip_logo.setImageResource(R.drawable.empty_no_network);
                bt_operate.setVisibility(View.VISIBLE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case refresh:
                relay_bg.setBackgroundColor(0xff000000);
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(VISIBLE);
                tv_refresh.setVisibility(VISIBLE);
                imgv_refresh.setImageResource(R.drawable.ic_reseti);
                tv_refresh.setTextColor(0xffffffff);
                break;
            case refresh_white:
                relay_bg.setBackgroundColor(0xffeeeeee);
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(VISIBLE);
                tv_refresh.setVisibility(VISIBLE);
                imgv_refresh.setImageResource(R.drawable.ic_reseti2);
                tv_refresh.setTextColor(Color.parseColor("#000000"));
                break;
            case loading:
                relay_bg.setBackgroundColor(0xffffffff);
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                tv_tips.setText("Loading...");
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case loading2:
                setVisibility(View.VISIBLE);
                relay_bg.setBackgroundColor(0xff000000);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                imgv_loading.setVisibility(GONE);
                tv_tips.setText("Loading...");
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case loading3:
                relay_bg.setBackgroundColor(0xffeeeeee);
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                tv_tips.setText("Loading...");
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.GONE);
                imgv_loading.setVisibility(GONE);

                imgv_refresh.setVisibility(GONE);
                tv_refresh.setVisibility(GONE);
                break;
            case lar:
                setVisibility(View.VISIBLE);
                img_tip_logo.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);
                bt_operate.setVisibility(View.GONE);
                tv_tips.setVisibility(View.GONE);
                tv_lar.setVisibility(View.VISIBLE);
                break;
            case finish:
                setVisibility(View.GONE);
                break;
        }
    }


    public void setOnReloadListener(onReloadListener onReloadListener) {
        this.onReloadListener = onReloadListener;

    }

    public void setOnReloadImgListener(onReloadImgListener onReloadImgListener) {
        this.onReloadImgListener = onReloadImgListener;

    }

    public void setLarListener(onLarListener onLarListener) {
        this.onLarListener = onLarListener;

    }


    public interface onReloadListener {
        void reload();
    }

    public interface onReloadImgListener {
        void reloadImg();
    }

    public interface onLarListener {
        void lar();
    }
}

