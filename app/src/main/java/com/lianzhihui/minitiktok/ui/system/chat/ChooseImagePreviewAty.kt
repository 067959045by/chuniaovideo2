package com.lianzhihui.minitiktok.ui.system.chat

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.echofeng.common.utils.AESFileCrypt
import com.lianzhihui.minitiktok.base.ImageLoader
import com.lianzhihui.onlyleague.R
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import java.io.File

class ChooseImagePreviewAty : AutoLayoutActivity(){

    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var tv_right:TextView?=null
    var recyclerview:RecyclerView?=null
    var list = ArrayList<String>()
    var index =0


     fun initView() {
        list = intent.getStringArrayListExtra("list") as ArrayList<String>
        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        tv_right =findViewById(R.id.tv_right)
        recyclerview =findViewById(R.id.recyclerview)

        initTopview(relay_top_bg,"#00000000")
        tv_title?.text= "预览"
        tv_right?.text = "${index+1}/${list.size}"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        tv_right?.setTextColor(Color.parseColor("#ffffff"))
        tv_right?.visibility = View.VISIBLE

    }

    //非沉淀状态栏   背景-资源文件
    fun initTopview(relay: RelativeLayout?,bgColor:String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(130)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aty_choose_img_previdew)
        initView()
        var linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL;
        recyclerview?.layoutManager = linearLayoutManager
        var mAdapter = GoldRecyclerAdapter(this)
        recyclerview?.adapter = mAdapter
    }

    fun mainClick(v:View){

        when(v.id){
            R.id.relay_back->{
                finish()
            }
        }
    }

    inner class GoldRecyclerAdapter(context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_choose_img_previdew, parent, false))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int = position

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if ( holder is fGoldViewHolder){
                Log.e("ssssssss",list[position])
                loadImg(list[position],holder.imgv!!)
            }

        }

         fun loadImg(imgUrl: String, imgv_msg: ImageView) {

            if(imgUrl.startsWith("http")){
//                imgv_msg.visibility = View.GONE
//                loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.loading3)
                val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                    override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                        val decryptBytes = AESFileCrypt.decryptData(resource)
                        val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
//                    imgv_head.setImageBitmap(bitmap)
                        try {
                            imgv_msg.visibility = View.VISIBLE
                            val width = bitmap.width
                            val height = bitmap.height
//                            UtilsImg.setImagViewW_H(imgv_msg, height, width)
                            imgv_msg.setImageBitmap(bitmap)

                        } catch (e2: Exception) {
                        }
                    }


                }
                Glide.with(this@ChooseImagePreviewAty).asFile().load(imgUrl).into(simpleTarget)
            }else{
                ImageLoader.loadImage(this@ChooseImagePreviewAty,imgUrl,imgv_msg)
            }

        }




        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv: ImageView? = null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
            }
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
            var mLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
            val pagerSnapHelper = PagerSnapHelper()
            pagerSnapHelper.attachToRecyclerView(recyclerView)
            recyclerView.setOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val position: Int = mLayoutManager!!.findFirstCompletelyVisibleItemPosition()
                    if (position >= 0 && index != position) {
                        index = position
                        tv_right?.text = "${index+1}/${list.size}"
                    }
                }
            })
        }

    }



}