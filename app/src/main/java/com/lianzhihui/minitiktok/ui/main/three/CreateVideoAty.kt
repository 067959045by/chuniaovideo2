package com.lianzhihui.minitiktok.ui.main.three

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.lianzhihui.minitiktok.base.BaseAty
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.aty_all_top.*

class CreateVideoAty:BaseAty() {


    override fun getLayoutId()= R.layout.aty_create_video


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initTopview(relay_top_bg, "#00000000")
        tv_title?.text = "上传视频"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))

    }

    fun mainClick(v: View) {
        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

        }
    }
}