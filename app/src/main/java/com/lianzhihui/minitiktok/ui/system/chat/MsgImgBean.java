package com.lianzhihui.minitiktok.ui.system.chat;


public class MsgImgBean extends MsgBean {



    private String newMessage;
    private String newMessageId;
    private int imgWidth;
    private int imgHeight;


    public MsgImgBean(MsgState status, MsgShowType type, String message, String messageId, String timestamp) {
        super(status, type, message, messageId, timestamp);
    }

    public int getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(int imgWidth) {
        this.imgWidth = imgWidth;
    }

    public int getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(int imgHeight) {
        this.imgHeight = imgHeight;
    }

    public String getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public String getNewMessageId() {
        return newMessageId;
    }

    public void setNewMessageId(String newMessageId) {
        this.newMessageId = newMessageId;
    }

    @Override
    public String toString() {
        return "MsgImgBean{" +
                "newMessage='" + newMessage + '\'' +
                ", newMessageId='" + newMessageId + '\'' +
                ", imgWidth=" + imgWidth +
                ", imgHeight=" + imgHeight +
                '}';
    }

}

