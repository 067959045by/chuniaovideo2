package com.lianzhihui.minitiktok.ui.main.three

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.by.net.JSONUtils
import com.lianzhihui.minitiktok.base.BaseAty
import com.lianzhihui.minitiktok.base.ImageLoader
import com.lianzhihui.minitiktok.interfaces.Home
import com.lianzhihui.minitiktok.ui.main.three.choseimg.ChooseImgVideoAty
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.aty_all_top.*
import kotlinx.android.synthetic.main.aty_create_cooper.*
import org.xutils.http.RequestParams

class CreateCooperAty:BaseAty() {

    val CHOOSE_IMG = 100
    var listImg = ArrayList<String>()

    var home = Home()

    fun mainClick(v: View){
        when(v.id){

            R.id.relay_back->{
                finish()
            }

            R.id.imgv_add->{
                var bundle = Bundle()
                bundle.putInt("max_num",1)
                bundle.putString("type","img")
                startActivityForResult(ChooseImgVideoAty::class.java,bundle,CHOOSE_IMG)
            }

            R.id.relay_ok->{
                if (listImg.size==0){
                    ToastUtils.showShort("请选择图片！")
                    return
                }
                if (TextUtils.isEmpty(edit_title.text)){
                    ToastUtils.showShort("请填写标题！")
                    return
                }
                startProgressDialog()
                home.a(listImg[0],this)
            }
        }
    }


    override fun onComplete(var1: RequestParams?, var2: String?, type: String?) {
        super.onComplete(var1, var2, type)

        if (type == "file/upload"){
            var map = JSONUtils.parseKeyAndValueToMap(var2)
            Log.e("onComplete=",map.toString())
            Log.e("onComplete2=",map["code"])
            if (map["code"]=="0"){
                var dataMap = JSONUtils.parseDataToMap(var2)
                home.a1(edit_title.text.toString(), dataMap["url"]!!,this)

            }else{
                ToastUtils.showShort("上传失败,请重试")
            }
        }

        if (type == "create/cooper"){
            stopProgressDialog()
            val map = JSONUtils.parseKeyAndValueToMap(var2)
            if (map["code"]!="0"){
                ToastUtils.showShort(map["message"])
            }else{

            }
        }
    }

    override fun onExceptionType(var1: Throwable?, params: RequestParams?, type: String?) {
        super.onExceptionType(var1, params, type)
        stopProgressDialog()
        ToastUtils.showShort("上传失败，请重试")
    }

    override fun getLayoutId()=R.layout.aty_create_cooper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg,"#00000000")
        tv_title?.text= "合集名称"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                100 ->{
                    var type =data?.getStringExtra("type")

                    when (type) {
                        "img_photo" -> {
                            var list = data?.getStringArrayListExtra("data")
                            if (list!=null){
                                listImg.clear()
                                listImg.addAll(list)
                            }
                            ImageLoader.loadImage(this@CreateCooperAty,listImg[0],imgv_add)
                        }
                        "camera" -> {
//                            var cameraImgPath = data?.getStringExtra("data")
//                            edit_01?.setText(cameraImgPath)
//                            ImageLoader.loadImage(this@TestChooseImgAty,cameraImgPath,imgv_01)
                        }
                    }
                }
            }

        }
    }

}