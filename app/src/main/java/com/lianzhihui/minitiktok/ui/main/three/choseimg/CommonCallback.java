package com.lianzhihui.minitiktok.ui.main.three.choseimg;

/**
 * Created by cxf on 2017/8/11.
 */

 interface CommonCallback<T> {
      void callback(T bean);
}
