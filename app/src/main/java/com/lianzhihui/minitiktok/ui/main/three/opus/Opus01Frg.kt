package com.lianzhihui.minitiktok.ui.main.three.opus

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lianzhihui.minitiktok.base.BaseFrg
import com.lianzhihui.minitiktok.base.ImageLoader
import com.lianzhihui.minitiktok.base.XLoadTip
import com.lianzhihui.minitiktok.interfaces.Home
import com.lianzhihui.onlyleague.R
import com.zhy.autolayout.utils.AutoUtils
import kotlinx.android.synthetic.main.frg_opus01.*
import org.xutils.http.RequestParams

class Opus01Frg:BaseFrg() {

    var page =1;

    var home = Home()

    override fun getLayoutId(): Int= R.layout.frg_opus01


    override fun initView() {
    }

    override fun requestData() {
        loading.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a2("",page,this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var dapter = GoldRecyclerAdapter(activity!!)
        recyclerView.layoutManager = GridLayoutManager(activity,3)
        recyclerView.adapter = dapter
    }

    override fun onComplete(var1: RequestParams?, var2: String?, type: String?) {
        super.onComplete(var1, var2, type)

        if (type== "cooper/lists"){


        }
    }

    override fun onExceptionType(var1: Throwable?, params: RequestParams?, type: String?) {
        super.onExceptionType(var1, params, type)
        loading.setLoadingTip(XLoadTip.LoadStatus.error)

    }

    inner class GoldRecyclerAdapter(context: Context) :

        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View = inflater.inflate(R.layout.item_opus01, parent, false)
            return fGoldViewHolder(view)
        }


        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if(holder is fGoldViewHolder){
                with(holder){
                    var url = "https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2021%2F0114%2Fea09bd73p00qmxkvg008wc000fk00f4m.png&thumbnail=650x2147483647&quality=80&type=jpg"
                    ImageLoader.loadImage(activity, url, imgv)
                }
            }

        }

        override fun getItemCount(): Int = 10

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv:ImageView? =null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
            }
        }

    }

}