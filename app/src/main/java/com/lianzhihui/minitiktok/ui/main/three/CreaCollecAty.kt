package com.lianzhihui.minitiktok.ui.main.three

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lianzhihui.onlyleague.R
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import kotlinx.android.synthetic.main.aty_all_top.*
import kotlinx.android.synthetic.main.aty_creation_collec.*

class CreaCollecAty : AutoLayoutActivity() {


    fun initTopview(relay: RelativeLayout?, bgColor: String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(150)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

    fun mainClick(v: View) {
        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.tv_new_collec ->{
                startActivity(Intent(this,CreateCooperAty::class.java))
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aty_creation_collec)
        initTopview(relay_top_bg, "#00000000")
        tv_title?.text = "合集名称"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))

        var dapter = GoldRecyclerAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = dapter
    }


    inner class GoldRecyclerAdapter(context: Context) :

        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View = inflater.inflate(R.layout.item_crea_collec, parent, false)
            return fGoldViewHolder(view)
        }


        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        }

        override fun getItemCount(): Int = 10

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            init {
                AutoUtils.autoSize(itemView)

            }
        }

    }

}