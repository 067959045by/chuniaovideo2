package com.lianzhihui.minitiktok.ui.main.three.opus

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.lianzhihui.minitiktok.base.BaseAty
import com.lianzhihui.minitiktok.ui.main.three.CreateVideoAty
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.aty_all_top.*

class OpusManageAty : BaseAty() {


    override fun getFragmentContainerId(): Int  = R.id.framlay_content

    override fun getLayoutId() = R.layout.aty_opus_manger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg, "#00000000")
        tv_title?.text = "作品管理"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        addFragment(Opus01Frg::class.java,null)
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.linlay_01 -> {
                addFragment(Opus01Frg::class.java,null)
            }

            R.id.linlay_04 -> {
                addFragment(Opus04Frg::class.java,null)
            }

            R.id.imgv_create ->{
                startActivity(CreateVideoAty::class.java)
            }

        }

    }


}