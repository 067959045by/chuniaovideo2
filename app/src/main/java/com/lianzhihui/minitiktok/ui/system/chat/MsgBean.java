package com.lianzhihui.minitiktok.ui.system.chat;


public class MsgBean {


    private String message;
    private String messageId;
    private String timestamp;
    private MsgShowType msgShowType;
    private MsgState status;
    private String userInfo;
    private String system;
    private String avatar;

    public MsgShowType getMsgShowType() {
        return msgShowType;
    }

    public void setMsgShowType(MsgShowType msgShowType) {
        this.msgShowType = msgShowType;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public MsgBean(MsgState status, MsgShowType msgShowType, String message, String messageId, String timestamp) {
        this.message = message;
        this.messageId = messageId;
        this.timestamp = timestamp;
        this.msgShowType = msgShowType;
        this.status = status;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public MsgState getStatus() {
        return status;
    }

    public void setStatus(MsgState status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public MsgShowType getType() {
        return msgShowType;
    }

    public void setType(MsgShowType type) {
        this.msgShowType = msgShowType;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "MsgBean{" +
                "message='" + message + '\'' +
                ", messageId='" + messageId + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", msgShowType=" + msgShowType +
                ", status=" + status +
                ", userInfo='" + userInfo + '\'' +
                ", system='" + system + '\'' +
                '}';
    }
}

