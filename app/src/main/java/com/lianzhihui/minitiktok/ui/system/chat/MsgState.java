package com.lianzhihui.minitiktok.ui.system.chat;

public enum MsgState {
    LOADING, //发送中
    SUCCESS, //发送/接受成功
    ERROR,   //发送失败
    BACK     //消息撤回
}
