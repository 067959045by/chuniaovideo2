package com.lianzhihui.minitiktok.ui.system.chat;

public enum MsgShowType {

    SEND_TEXT,   //发送的文本
    SEND_IMG,    //发送的图片
    RETURN_TEXT, //接受的文本
    RETURN_IMG,  //接受的图片
    RETURN_SYM,  //接受系统消息

}
