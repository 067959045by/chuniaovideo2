package com.lianzhihui.minitiktok.ui.system.chat

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.ToastUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.by.MyRefreshAndLoadListen
import com.echofeng.common.config.AppConfig
import com.echofeng.common.config.DataManager
import com.echofeng.common.net.*
import com.echofeng.common.utils.*
import com.lianzhihui.minitiktok.bean.system.UploadResponse
import com.lianzhihui.onlyleague.R
import com.net.core.EasyHttp
import com.net.core.body.UIProgressResponseCallBack
import com.net.core.callback.ProgressDialogCallBack
import com.net.core.exception.ApiException
import com.net.core.model.HttpParams
import com.net.core.subsciber.IProgressDialog
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import kotlinx.android.synthetic.main.aty_all_top.*
import kotlinx.android.synthetic.main.aty_chat.*
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

class ChatAty:AutoLayoutActivity() {


    var list_all = ArrayList<MsgBean>()
    var adapter : GoldRecyclerAdapter?=null

    fun initTopview(relay: RelativeLayout?,bgColor:String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(130)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

    fun mainClick(v:View){
        when(v.id){
            R.id.relay_back->{
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aty_chat)
        initTopview(relay_top_bg,"#00000000")
        tv_title?.text= "客服"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        adapter = GoldRecyclerAdapter(this)
        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.adapter = adapter

        imgv_send.setOnClickListener {
            if (TextUtils.isEmpty(edit_chat.text.toString())) {
                ToastUtils.showShort("发送内容不能为空")
                return@setOnClickListener
            }
            val time_send = (System.currentTimeMillis()/1000).toString()
            val msgBean = MsgBean(MsgState.SUCCESS, MsgShowType.SEND_TEXT, edit_chat.text.toString(), "msgId", time_send)
            list_all.add(msgBean)
            msgBean.avatar = PreferenceUtils.getString("head_my","")
            adapter?.notifyItemChanged(list_all.size - 1)
            recyclerview.scrollToPosition(adapter?.itemCount!! - 1)
            edit_chat.setText("")
            doSendMessage("1",msgBean.message)
        }

        imgv_img.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(intent, 1314)
        }

        doGetList("")
        swipeRefreshLayout.setEnableLoadmore(true)
        swipeRefreshLayout.setMyRefreshAndLoadListen(object : MyRefreshAndLoadListen {
            override fun refreshStart() {
//                mine?.c14(this@ChatAty,page+1,order_id)
                var ids = ""
                if (list_all!=null&&list_all.size>0){
                    if (!TextUtils.isEmpty(list_all[0].messageId)){
                        ids =list_all[0].messageId
                    }
                }
                doGetList(ids)
            }

            override fun loadMoreStart() {
                doGetList("")
            }
        })
    }

    fun doGetList( ids:String) {
        Log.e("ids==",ids)
        val params = HttpParams()
        params.put("page", "1")
        params.put("page_size", "10")
        params.put("id", ids)

        HttpManager(this, object : HttpManagerCallback() {
            override fun onSuccess(o: ResultData) {
                swipeRefreshLayout.finishRefreshing()
                swipeRefreshLayout.finishLoadmore()
                if (TextUtils.isEmpty(ids)){
                    list_all.clear()
                }
                val decryptJson = AESCBCCrypt2.aesDecrypt(o.getData())
                Log.e("doGetList=",decryptJson)
                var mapList =JSONUtils.parseKeyAndValueToMapList(decryptJson)
                val arrayList = ArrayList<MsgBean>()

                for (map in mapList){

                    var msgBean:MsgBean?=null
                    if (TextUtils.isEmpty(map.get("rid"))){
                        if (map.get("type").equals("1")){
                             msgBean = MsgBean(MsgState.SUCCESS, MsgShowType.SEND_TEXT, map.get("content"), "msgId", "time_send");
                        }else{
                             msgBean = MsgImgBean(MsgState.SUCCESS, MsgShowType.SEND_IMG, map.get("content"), "msgId", "time_send");
                        }
                        msgBean.avatar = map.get("avatar")
                        msgBean.messageId = map.get("id")

                    }else{
                        if (map.get("type").equals("1")){
                            msgBean = MsgBean(MsgState.SUCCESS, MsgShowType.RETURN_TEXT, map.get("content"), "msgId", "time_send");
                        }else{
                            msgBean = MsgImgBean(MsgState.SUCCESS, MsgShowType.RETURN_IMG, map.get("content"), "msgId", "time_send");
                        }
                        msgBean.avatar = map.get("avatar")
                        msgBean.messageId = map.get("id")

                    }
                    arrayList.add(msgBean!!)
                }

                list_all.addAll(0, arrayList)
                adapter?.notifyDataSetChanged()
                if (TextUtils.isEmpty(ids)){
                    recyclerview.scrollToPosition(adapter?.itemCount!! - 1)

                }else{
                    if (list_all.size>mapList.size){
                        recyclerview.scrollToPosition(mapList.size)
                    }else{
                        recyclerview.scrollToPosition(mapList.size-1)
                    }
                }


            }

            override fun onError(e: ApiException) {
                swipeRefreshLayout.finishRefreshing()
                swipeRefreshLayout.finishLoadmore()
//                MyToast.showToast(e.message)
            }
        }).post(APIConstant.GET_CHAT_LIST, params)
    }


    fun doSendMessage(type: String?, content: String?) {
        val params = HttpParams()
        params.put("type", type)
        params.put("content", content)
        HttpManager(this, object : HttpManagerCallback() {
            override fun onSuccess(o: ResultData) {
                ToastUtils.showShort("发送成功")
                recyclerview.scrollToPosition(adapter?.itemCount!! - 1)

            }

            override fun onError(e: ApiException) {
                MyToast.showToast(e.message)
            }
        }).post(APIConstant.DO_SEND_MESSAGE, params)
    }


    fun doImg( file:File){
        doUploadHead(file,object : UIProgressResponseCallBack() {
            override fun onUIResponseProgress(bytesRead: Long, contentLength: Long, done: Boolean) {
                if (done) {
//                    MyToast.showToast("上传成功")
                }
            }
        })
    }
    fun doUploadHead(file: File, listener: UIProgressResponseCallBack?) {
        val mProgressDialog = IProgressDialog {
            val dialog = ProgressDialog(this)
            dialog.setMessage("发送中...")
            dialog
        }

        val timeStamp: String = (System.currentTimeMillis() / 1000).toString() + ""
        val singPostBody = DataManager.singFileBody(timeStamp + "")
        EasyHttp.post(AppConfig.upload_url + "/upload?" + "platform=A&timestamp=" + timeStamp + "&dir=user&sign=" + singPostBody) //方式一：文件上传
            //如果有文件名字可以不用再传Type,会自动解析到是image/*
            .headers(HttpManager.getHeaders())
            .params("file", file, file.name, listener)
            .accessToken(true)
            .timeStamp(true)
            .execute(object : ProgressDialogCallBack<String?>(mProgressDialog, true, true) {
                override fun onError(e: ApiException) {
                    super.onError(e)
                    MyToast.showToast(e.message)
                }

                override fun onSuccess(response: String?) {
                    val resultData = GsonUtil.GsonToBean(response, ResultDataNoAes::class.java)
                    val data = GsonUtil.GsonToBean(resultData.data, UploadResponse::class.java)
                    doSendMessage("2", data.file_path)
                }
            })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 1314) {
                val uri = data!!.data
                val cr = this.contentResolver
                try {
                    val bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri!!))
                    val bitmap2 = FileCompressUtil.compressImage(bitmap)
                    var file: File? = null
                    try {
                        file = File(FileCompressUtil.saveFile(bitmap2, FileCompressUtil.getTimeNow() + "_" + 0, this@ChatAty))
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    val time_send = System.currentTimeMillis()
                    val msgBean = MsgImgBean(MsgState.SUCCESS, MsgShowType.SEND_IMG, file!!.absolutePath,"", time_send.toString() + "")
                    msgBean.avatar = PreferenceUtils.getString("head_my","")

                    list_all.add(msgBean)
//                    map_send[StringUtils.getMD5Str(file.absolutePath + time_send + AppConfig.userId)] = list_all.size - 1
//                    SocketImClient.getInstance(this@ChatAty,socketService).initInterface(this@ChatAty).sendImg(file.absolutePath, time_send,order_id)
                    if (file != null) {
                        doImg(file)
                    }
                    adapter?.notifyDataSetChanged()
                    recyclerview.scrollToPosition(adapter?.itemCount!! - 1)
                    edit_chat.setText("")

                } catch (e: FileNotFoundException) {
                }

            }
        }
    }


    inner class GoldRecyclerAdapter(context: Context) :

        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        private val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View
            when (viewType) {
                //发送的文本
                1 -> {
                    view = inflater.inflate(R.layout.item_chat_my_send, parent, false)
                    return fGoldViewHolder(view)
                }
                //发送的图片
                11->{
                    view = inflater.inflate(R.layout.item_chat_my_send_img, parent, false)
                    return fGoldViewHolderImg(view)
                }

                //收到的图片
                22->{
                    view = inflater.inflate(R.layout.item_chat_other_send_img, parent, false)
                    return fGoldViewHolder2Img(view)
                }
                //收到的文本
                else ->{
                    view = inflater.inflate(R.layout.item_chat_other_send, parent, false)
                    return fGoldViewHolder2(view)
                }
            }
        }

        override fun getItemViewType(position: Int): Int {
            val type = list_all[position].type
            when (type) {
                MsgShowType.SEND_TEXT -> return 1
                MsgShowType.SEND_IMG -> return 11
                MsgShowType.RETURN_IMG -> return 22
                else -> return 2
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            //发送的消息
            if (holder is fGoldViewHolder) {

                with(holder){
                    val msgBean = list_all[position]
                    val status = msgBean.status

//                    if (position > 0) {
//                        val preMsgTime = java.lang.Long.parseLong(list_all[position - 1].timestamp)*1000
//                        val cruMsgTime = java.lang.Long.parseLong(msgBean.timestamp)*1000
//
//                        if (cruMsgTime - preMsgTime > 1000 * 60 * 10) {
//                            tv_time.visibility = View.VISIBLE
//                            tv_time.text = TimeUtils.getStrTime(msgBean.timestamp)
//                        } else {
//                            tv_time.visibility = View.GONE
//                        }
//                    }
                    tv_msg.text = msgBean.message
                    var head_url = msgBean.avatar
//                    tv_name.text =PreferencesUtils.getString(this@ChatAty,"nick_name")
//                    var head_url = PreferencesUtils.getString(this@ChatAty, "head_url")
//                    if (!head_url.startsWith("http")) head_url = AppConfig.Host_Url + head_url
                    if (!TextUtils.isEmpty(head_url)){
                        val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                            override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                                val decryptBytes = AESFileCrypt.decryptData(resource)
                                val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
//                                imgv_head.setImageBitmap(R.id.ivHead, bitmap)
                                imgv_head.setImageBitmap(bitmap)
                            }
                        }
                        Glide.with(this@ChatAty).asFile().load(head_url).override(100, 100).error(R.mipmap.ic_logo).into(simpleTarget)
                    }

                    when (status) {
                        //发送中
                        MsgState.LOADING -> {
                            progress.visibility = View.VISIBLE
                            imgv_failed.visibility = View.GONE
                        }
                        //发送成功
                        MsgState.SUCCESS -> {
                            progress.visibility = View.GONE
                            imgv_failed.visibility = View.GONE
                        }
                        //发送失败
                        MsgState.ERROR -> {
                            progress.visibility = View.GONE
                            imgv_failed.visibility = View.VISIBLE
                        }
                    }
                    imgv_failed.setOnClickListener { v ->
//                        if (!StringUtils.isNetConnected(this@ChatAty)) return@setOnClickListener
                        val msgBean2 = list_all[position]
                        msgBean2.status = MsgState.LOADING
                        list_all[position] = msgBean2
                        adapter?.notifyItemChanged(position)
//                        socketService?.sendMsg(MsgUtils.getMsgGson(msgBean2.message,AppConfig.userId, order_id, msgBean2.timestamp),  msgBean2.timestamp)

                    }
                }

            }

            //收到的消息
            if (holder is fGoldViewHolder2) {

                with(holder){
                    val msgBean = list_all[position]
//                    if (position > 0) {
//                        val preMsgTime = java.lang.Long.parseLong(list_all[position - 1].timestamp)*1000
//                        val cruMsgTime = java.lang.Long.parseLong(msgBean.timestamp)*1000
//                        if (cruMsgTime - preMsgTime > 1000 * 60 * 10) {
//                            tv_time.visibility = View.VISIBLE
//                            tv_time.text = TimeUtils.getStrTime(msgBean.timestamp)
//                        } else {
//                            tv_time.visibility = View.GONE
//                        }
//                    }
                    if (msgBean.system=="2"){
                        relay_bg.visibility=View.GONE
                        tv_close.visibility =View.VISIBLE
                        tv_close.text =msgBean.message
                    }else{
                        relay_bg.visibility=View.VISIBLE
                        tv_close.visibility =View.GONE
                    }

                    var head_url = msgBean.avatar
                    if (!TextUtils.isEmpty(head_url)){
                        val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                            override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                                val decryptBytes = AESFileCrypt.decryptData(resource)
                                val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
                                imgv_head.setImageBitmap(bitmap)
                            }
                        }
                        Glide.with(this@ChatAty).asFile().load(head_url).override(100, 100).error(R.mipmap.ic_logo).into(simpleTarget)
                    }
                    tv_content.visibility = View.GONE
                    relay_content.visibility = View.VISIBLE
                    tv_msg.text =msgBean.message

//                    val split = msgBean.userInfo.split(",")
//                    tv_name.text=split[1]
//                    var head_url =split[0]
//                    if (!head_url.startsWith("http")) head_url = AppConfig.Host_Url + head_url
//                    if (TextUtils.isEmpty(head_url)) ImagesUtils.disImgCircleNo(this@ChatAty, R.drawable.test, imgv_head) else ImagesUtils.disImgCircleNo(this@ChatAty, head_url, imgv_head)

                }

            }

            //发送的图片
            if(holder is fGoldViewHolderImg){
                with(holder){
                    val msgBean = list_all[position]
                    val status = msgBean.status

                    tv_content.visibility = View.GONE
                    relay_content.visibility = View.VISIBLE
//                    tv_name.text =PreferencesUtils.getString(this@ChatAty,"nick_name")
//                    var head_url = PreferencesUtils.getString(this@ChatAty, "head_url")
//                    if (!head_url.startsWith("http")) head_url = AppConfig.Host_Url + head_url
//                    if (TextUtils.isEmpty(head_url)) ImagesUtils.disImgCircleNo(this@ChatAty, R.drawable.test, imgv_head) else ImagesUtils.disImgCircleNo(this@ChatAty, head_url, imgv_head)

                    if (msgBean is MsgImgBean) {

                        var imgUrl = msgBean.getMessage()
//                        else{
//                            if (!msgBean.newMessage.startsWith("http")) msgBean.newMessage else msgBean.newMessage
//                        }

                        imgv_msg.visibility = View.VISIBLE
                        loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.loading3)
                        loadedTip_img.setOnReloadImgListener {
//                            loadImg(imgUrl, imgv_msg, loadedTip_img)
                        }
                        var head_url = msgBean.avatar
                        if (!TextUtils.isEmpty(head_url)){
                            val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                                override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                                    val decryptBytes = AESFileCrypt.decryptData(resource)
                                    val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
                                    imgv_head.setImageBitmap(bitmap)
                                }
                            }
                            Glide.with(this@ChatAty).asFile().load(head_url).override(100, 100).error(R.mipmap.ic_logo).into(simpleTarget)
                        }

                        loadImg(imgUrl, imgv_msg, loadedTip_img)

                        imgv_msg.setOnClickListener {
                            var list = ArrayList<String>()
                            list.add(imgUrl)
                            val bundle = Bundle()
                            bundle.putStringArrayList("list", list)
                            var intent = Intent(this@ChatAty,ChooseImagePreviewAty::class.java)
                            intent.putExtras(bundle)
                            this@ChatAty.startActivity(intent)
                        }


                        when (status) {
                            //发送中
                            MsgState.LOADING -> {
                                progress.visibility = View.VISIBLE
                                imgv_failed.visibility = View.GONE
                            }
                            //发送成功
                            MsgState.SUCCESS -> {
                                progress.visibility = View.GONE
                                imgv_failed.visibility = View.GONE
                            }
                            //发送失败
                            MsgState.ERROR -> {
                                progress.visibility = View.GONE
                                imgv_failed.visibility = View.VISIBLE
                            }
                        }
                    }
                }
            }

            //收到的图片
            if (holder is fGoldViewHolder2Img){

                with(holder){
                    val msgBean = list_all[position]
                    if (msgBean is MsgImgBean) {
                        var imgUrl = msgBean.getMessage()

                        imgv_msg.visibility = View.VISIBLE
                        loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.loading3)
                        loadedTip_img.setOnReloadImgListener {

//                            loadImg(imgUrl, imgv_msg, loadedTip_img)
                        }

                        var head_url = msgBean.avatar
                        if (!TextUtils.isEmpty(head_url)){
                            val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                                override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                                    val decryptBytes = AESFileCrypt.decryptData(resource)
                                    val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
                                    imgv_head.setImageBitmap(bitmap)
                                }
                            }
                            Glide.with(this@ChatAty).asFile().load(head_url).override(100, 100).error(R.mipmap.ic_logo).into(simpleTarget)
                        }
                        loadImg(imgUrl, imgv_msg, loadedTip_img)
                        imgv_msg.setOnClickListener {
                            var list = ArrayList<String>()
                            list.add(imgUrl)
                            val bundle = Bundle()
                            bundle.putStringArrayList("list", list)
                            var intent = Intent(this@ChatAty,ChooseImagePreviewAty::class.java)
                            intent.putExtras(bundle)
                            this@ChatAty.startActivity(intent)
                        }


//                        imgv_msg.setOnClickListener {
//                            var list = ArrayList<String>()
//                            list.add(imgUrl)
//                            val bundle = Bundle()
//                            bundle.putStringArrayList("list", list)
//                            var intent = Intent(this@ChatAty,ChooseImagePreviewAty::class.java)
//                            intent.putExtras(bundle)
//                            this@ChatAty.startActivity(intent)
//                        }
//

//                        val split = list_all[position].userInfo.split(",")
//                        tv_name.text=split[1]
//                        var head_url =split[0]
//                        if (!head_url.startsWith("http")) head_url = AppConfig.Host_Url + head_url
//                        if (TextUtils.isEmpty(head_url)) ImagesUtils.disImgCircleNo(this@ChatAty, R.drawable.test, imgv_head) else ImagesUtils.disImgCircleNo(this@ChatAty, head_url, imgv_head)
                    }
                }
            }

        }

        private fun loadImg(imgUrl: String, imgv_msg: ImageView, loadedTip_img: LoadingTip) {

            if(imgUrl.startsWith("http")){
                imgv_msg.visibility = View.GONE
                loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.loading3)
                val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {

                    override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                        val decryptBytes = AESFileCrypt.decryptData(resource)
                        val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
//                    imgv_head.setImageBitmap(bitmap)
                        try {
                            imgv_msg.visibility = View.VISIBLE
                            val width = bitmap.width
                            val height = bitmap.height
                            UtilsImg.setImagViewW_H(imgv_msg, height, width)
                            imgv_msg.setImageBitmap(bitmap)
                            loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.finish)

                        } catch (e2: Exception) {
                        }
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
                        try {
                            imgv_msg.visibility = View.GONE
                            loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.refresh_white)
                        } catch (e2: Exception) {
                        }

                    }
                }
                Glide.with(this@ChatAty).asFile().load(imgUrl).into(simpleTarget)
            }else{
                Glide.with(this@ChatAty).asBitmap().load(imgUrl).into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(bitmap: Bitmap, transition: Transition<in Bitmap>?) {
                        try {
                            imgv_msg.visibility = View.VISIBLE
                            val width = bitmap.width
                            val height = bitmap.height
                            UtilsImg.setImagViewW_H(imgv_msg, height, width)
                            imgv_msg.setImageBitmap(bitmap)
                            loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.finish)
                        } catch (e2: Exception) {
                        }

                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        super.onLoadFailed(errorDrawable)
                        try {
                            imgv_msg.visibility = View.GONE
                            loadedTip_img.setLoadingTip(LoadingTip.LoadStatus.refresh_white)
                        } catch (e2: Exception) {
                        }

                    }
                })

            }

        }

        override fun getItemCount(): Int = list_all.size

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tv_msg = itemView.findViewById(R.id.tv_msg) as TextView
            var tv_name = itemView.findViewById(R.id.tv_name) as TextView
            var progress = itemView.findViewById(R.id.progress) as ProgressBar
            var imgv_failed = itemView.findViewById(R.id.imgv_failed) as ImageView
            var imgv_head = itemView.findViewById(R.id.imgv_head) as ImageView
            var relay_content = itemView.findViewById(R.id.relay_content) as RelativeLayout
            var tv_content = itemView.findViewById(R.id.tv_content) as TextView
            var tv_time = itemView.findViewById(R.id.tv_time) as TextView

            init {
                AutoUtils.autoSize(itemView)

            }
        }

        inner class fGoldViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tv_msg = itemView.findViewById(R.id.tv_msg) as TextView
            var tv_name = itemView.findViewById(R.id.tv_name) as TextView
            var imgv_head = itemView.findViewById(R.id.imgv_head) as ImageView
            var relay_content = itemView.findViewById(R.id.relay_content) as RelativeLayout
            var relay_bg = itemView.findViewById(R.id.relay_bg) as RelativeLayout
            var tv_content = itemView.findViewById(R.id.tv_content) as TextView
            var tv_time = itemView.findViewById(R.id.tv_time) as TextView
            var tv_close = itemView.findViewById(R.id.tv_close) as TextView
            init {
                AutoUtils.autoSize(itemView)

            }
        }

        inner class fGoldViewHolderImg(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv_msg = itemView.findViewById(R.id.imgv_msg) as ImageView
            var progress = itemView.findViewById(R.id.progress) as ProgressBar
            var imgv_failed = itemView.findViewById(R.id.imgv_failed) as ImageView
            var imgv_head = itemView.findViewById(R.id.imgv_head) as ImageView
            var tv_name = itemView.findViewById(R.id.tv_name) as TextView
            var relay_content = itemView.findViewById(R.id.relay_content) as RelativeLayout
            var tv_content = itemView.findViewById(R.id.tv_content) as TextView
            var loadedTip_img = itemView.findViewById(R.id.loadedTip_img) as LoadingTip
            init {
                AutoUtils.autoSize(itemView)
            }
        }

        internal inner class fGoldViewHolder2Img(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv_msg = itemView.findViewById(R.id.imgv_msg) as ImageView
            var imgv_head = itemView.findViewById(R.id.imgv_head) as ImageView
            var tv_name = itemView.findViewById(R.id.tv_name) as TextView
            var relay_bg = itemView.findViewById(R.id.relay_bg) as RelativeLayout
            var tv_content = itemView.findViewById(R.id.tv_content) as TextView
            var loadedTip_img = itemView.findViewById(R.id.loadedTip_img) as LoadingTip

            init {
                AutoUtils.autoSize(itemView)
            }
        }

    }

}