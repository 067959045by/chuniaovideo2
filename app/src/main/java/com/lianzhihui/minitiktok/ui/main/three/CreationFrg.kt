package com.lianzhihui.minitiktok.ui.main.three

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import com.blankj.utilcode.util.ToastUtils
import com.lianzhihui.minitiktok.base.BaseAty
import com.lianzhihui.minitiktok.base.BaseFrg
import com.lianzhihui.minitiktok.ui.main.three.opus.OpusManageAty
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.aty_all_top.*
import kotlinx.android.synthetic.main.aty_creation.*

class CreationFrg:BaseFrg() {


    override fun getLayoutId()=R.layout.aty_creation

    override fun initView() {
    }

    override fun requestData() {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_title?.text= "创作者中心"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        relay_right.visibility = View.VISIBLE
        relay_back.visibility = View.GONE
        imgv_right.setImageResource(R.drawable.crea_01)

        imgv_right.setOnClickListener {
            ToastUtils.showShort("我是搜索")
        }

        linlay_opus.setOnClickListener {
            startActivity(OpusManageAty::class.java)
        }

    }

}