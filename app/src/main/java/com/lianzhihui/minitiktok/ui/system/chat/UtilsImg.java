package com.lianzhihui.minitiktok.ui.system.chat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;

public class UtilsImg {


    public static ArrayList<Integer> getImgW_H(String path) {
        //获取Options对象
        ArrayList<Integer> list = new ArrayList<>();
        BitmapFactory.Options options = new BitmapFactory.Options();
//仅做解码处理，不加载到内存
        options.inJustDecodeBounds = true;
//解析文件
        BitmapFactory.decodeFile(path, options);
//获取宽高
        int imgWidth = options.outWidth;
        int imgHeight = options.outHeight;
        list.add(imgWidth);
        list.add(imgHeight);
        return list;
    }

    public static  Bitmap getBitmapByName(Context context, String name) {
        ApplicationInfo appInfo = context.getApplicationInfo();
        int resID = context.getResources().getIdentifier(name, "drawable", appInfo.packageName);
        return BitmapFactory.decodeResource(context.getResources(), resID);
    }

    public static  void  setImagViewW_H(ImageView  imgv,int h,int w){

        if (h==0||w==0){
            return;
        }
        int w1 = AutoUtils.getPercentWidthSize(500);
        int h1 = AutoUtils.getPercentHeightSize(500);
        int min = AutoUtils.getPercentHeightSize(200);
        if (w > h) {
            if (w > w1) {
                h = h * w1 / w;
                w = w1;
            }
        } else {
            if (h > h1) {
                w = w * h1 / h;
                h = h1;
            }
        }
        if (w < min && h < min) {
            if (w > h) {
                h = h * min / w;
                w = min;
            } else {
                w = w * min / h;
                h = min;
            }
        }
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)  imgv.getLayoutParams();
        params.width = w;
        params.height = h;
        imgv.setLayoutParams(params);
    }
}
