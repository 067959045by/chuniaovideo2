package com.lianzhihui.minitiktok.ui.main.three.choseimg

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blankj.utilcode.util.ToastUtils
import com.lianzhihui.minitiktok.base.BaseFrg
import com.lianzhihui.minitiktok.base.ImageLoader
import com.lianzhihui.onlyleague.R
import com.zhy.autolayout.utils.AutoUtils

class ChooseImgFrg:BaseFrg(),CommonCallback<MutableList<ChooseImageBean>> {


    var max_num =9
    var mCameraPath = ""//拍照后得到的图片路径
    var list = ArrayList<ChooseImageBean>()
    var list_check = ArrayList<String>()

    var recyclerview:RecyclerView?=null
    var chooseImageUtil: ChooseImageUtil? = null

    override fun getLayoutId(): Int= R.layout.frg_choose_img


    fun returnListCheck() =list_check

    override fun initView() {
        recyclerview =rootView?.findViewById(R.id.recyclerView)
        chooseImageUtil = ChooseImageUtil(activity)
    }

    override fun initRefreshData(data: Any?) {
        var map = data as HashMap<String, String>
        max_num = map["max_num"]!!.toInt()
    }

    override fun requestData() {
        chooseImageUtil?.getLocalImageList(this)
    }

    override fun callback(bean: MutableList<ChooseImageBean>?) {
        Log.e("callback ==",bean.toString())
        if (bean!=null&&bean.size>0){
            list.addAll(bean)
        }
        recyclerview?.layoutManager = GridLayoutManager(activity, 3)
        var decoration = ItemDecoration(activity, 0x00000000, 5F, 5F);
        decoration.isOnlySetItemOffsetsButNoDraw = true;
        recyclerview?.addItemDecoration(decoration)
        var mAdapter = GoldRecyclerAdapter(activity)
        recyclerview?.adapter = mAdapter
    }

    inner class GoldRecyclerAdapter(context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return fGoldViewHolder(inflater.inflate(R.layout.item_choose_img, parent, false))
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int = position

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                ImageLoader.loadImage(activity, list[position].imageFile.absolutePath, holder.imgv)

                if (list[position].isChecked){
                    holder.imgv_check?.setImageResource(R.drawable.icon_select_1)
                }else{
                    holder.imgv_check?.setImageResource(R.drawable.icon_select_0)
                }

                holder.itemView.setOnClickListener {

                    if (!list[position].isChecked&&list_check.size==max_num){
                        ToastUtils.showShort("最多选择${max_num}张图片")
                        return@setOnClickListener
                    }

                    list[position].isChecked = !list[position].isChecked

                    if (list[position].isChecked){
                        list_check.add(list[position].imageFile.absolutePath)
                    }else{
                        list_check.remove(list[position].imageFile.absolutePath)
                    }
                    notifyItemChanged(position)
                }
            }


        }

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv: ImageView? = null
            var imgv_check: ImageView? = null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
                imgv_check = itemView.findViewById(R.id.imgv_check)
            }
        }
    }


}