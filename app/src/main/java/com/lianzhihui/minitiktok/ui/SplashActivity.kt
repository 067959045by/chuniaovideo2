package com.lianzhihui.minitiktok.ui

import android.Manifest
import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.echofeng.common.config.AppConfig
import com.echofeng.common.config.AppStatusManager
import com.echofeng.common.ui.base.BaseNoRestartActivity
import com.echofeng.common.utils.*
import com.lianzhihui.minitiktok.bean.user.LoginResponse
import com.lianzhihui.minitiktok.config.AppConstants
import com.lianzhihui.minitiktok.presenter.LoginPresnterImp
import com.lianzhihui.minitiktok.utils.MyUtils
import com.lianzhihui.minitiktok.view.LoginView
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.activity_splash.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class SplashActivity : BaseNoRestartActivity(), LoginView, View.OnClickListener {

    private var pastChannel: String = ""
    private var pastCode: String = ""
    private lateinit var verificationCountDownTimer: VerificationCountDownTimer
    private lateinit var loginPresnterImp: LoginPresnterImp

    private val REQUESTCODE = 1001
    private var mPermissionList = ArrayList<String>()
    private val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private var isEndbleOpen = false


    fun requestPermission() {
        mPermissionList.clear()
        for (i in PERMISSIONS_STORAGE.indices) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    PERMISSIONS_STORAGE[i]
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                mPermissionList.add(PERMISSIONS_STORAGE[i])
            }
        }
        if (mPermissionList.isEmpty() || mPermissionList.size == 0 || Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            isEndbleOpen = true
            startdoCheckIp()
        } else {
            val permissions = mPermissionList.toTypedArray() //将List转为数组
            ActivityCompat.requestPermissions(this, permissions, REQUESTCODE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUESTCODE) {
            for (i in grantResults.indices) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    val showRequestPermission: Boolean =
                        ActivityCompat.shouldShowRequestPermissionRationale(
                            this,
                            permissions[i].toString()
                        )
                    if (showRequestPermission) {
                        isEndbleOpen = false
                        requestPermission()
                    } else {
                        isEndbleOpen = false
                        val builder = AlertDialog.Builder(this)
                        builder.setCancelable(false)
                        builder.setTitle("开启权限")
                        builder.setMessage("为了您更好的使用软件需要开启权限:" + "\n\n" + "电话权限")
                        builder.setPositiveButton("开启") { dialog, which ->
                            val i = Intent()
                            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package", packageName, null)
                            i.data = uri
                            startActivity(i)
                            dialog.dismiss()
                        }
                        var dialog = builder.show()
                    }
                } else {
                    isEndbleOpen = true
                }
            }
            if (isEndbleOpen) {
                startdoCheckIp()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash;
    }

    override fun initView() {
        tvJump.setOnClickListener(this)
        ivCover.setOnClickListener(this)
        loginPresnterImp = LoginPresnterImp(this, this)
//        showLoading("登录中...")
//        e_01.setText("http://download.chuniao001.me/download_android.html?code=&channel=xn756019")
        get_copy2()
        Handler().postDelayed({
            run {
                get_copy()
            }
        }, 1000)
    }

    fun startdoCheckIp() {
        showLoading("登录中...")
        loginPresnterImp.doCheckIp()
    }

    override fun initData() {
        var timeIndex = 5*1000
        if (AppConfig.debug){
            timeIndex = 1*1000
        }
        verificationCountDownTimer = object : VerificationCountDownTimer(timeIndex.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                super.onTick(millisUntilFinished)
                runOnUiThread {
                    tvJump.setText((millisUntilFinished / 1000).toString() + "S")
                    tvJump.setEnabled(false)
                }
            }

            override fun onFinish() {
                super.onFinish()
                runOnUiThread {
                    tvJump.setText("跳过")
                    tvJump.setEnabled(true)
                }
            }
        }
    }

    var textXX = ""
    private fun initPast() {
        var clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        if (!clipboard.hasPrimaryClip()) {
            return;
        }
        var clipData = clipboard.getPrimaryClip() as ClipData
        if (clipData != null && clipData.getItemCount() > 0) {
            var text = clipData.getItemAt(0).getText().toString()
            textXX = text
            if (text.contains("code=") && text.contains("channel=")) {
                pastCode = MyUtils.getCode(text)
                pastChannel = MyUtils.getChannel(text)
            }
        }
        requestPermission()

    }

    fun get_copy() {
        var content = "";
        var clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

        try {
            var clipData = clipboard.primaryClip;
            var item = clipData?.getItemAt(0);
            content = item?.text.toString();
            textXX = content
        } catch (e: Exception) {

        } finally {

            if (content.contains("code=") && content.contains("channel=")) {
                pastCode = MyUtils.getCode(content)
                pastChannel = MyUtils.getChannel(content)
            }
            requestPermission()
        }
    }

    fun get_copy2(){

        var content = "";
        try {
            var clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            var clipData = clipboard.primaryClip;
            var item = clipData?.getItemAt(0);
            content = item?.text.toString();
        } catch (e: Exception) {

        }finally {


        }

    }


    override fun setSuccess(o: Any?) {

        dismissLoading()
        if (TextUtils.isEmpty(pastChannel)) {
            pastChannel = SystemUtils.getChannelName()
        }

//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("test")
//        builder.setMessage("当前的渠道:"+pastChannel+"\n\n粘贴板内容:"+textXX)
//        builder.show()
        Log.e("TIKTOK_CHANNEL", pastCode + "当前的渠道为:" + pastChannel)
        loginPresnterImp.doLogin(pastCode, pastChannel)

    }


    override fun setFailure(o: Any?) {
        dismissLoading()
    }

    override fun setLoginSuccess(data: LoginResponse) {
        dismissLoading()
        ivCover.setTag(data.startup.link)
        AppConfig.upload_url = data.config.sys.upload_url
        PreferenceUtils.applyString("app_store", data.config.sys.app_store)
        val simpleTarget: SimpleTarget<File?> = object : SimpleTarget<File?>() {
            override fun onResourceReady(resource: File, transition: Transition<in File?>?) {
                val decryptBytes = AESFileCrypt.decryptData(resource)
                val bitmap = BitmapFactory.decodeByteArray(decryptBytes, 0, decryptBytes.size)
                ivCover.setImageBitmap(bitmap)
                tvJump.visibility = View.VISIBLE
            }
        }
        Handler().postDelayed({
            Glide.with(context!!).asFile().load(data!!.startup.cover).into(simpleTarget)
            verificationCountDownTimer.start()
        }, 2000)
    }

    override fun setBindPhoneSuccess(data: Any?) {
    }

    override fun onClick(v: View?) {
        when (v) {
            tvJump -> {
                AppStatusManager.getInstance().setAppStatus(AppConstants.AppStatus.STATUS_NORMAL)
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
            ivCover -> {
                var linkUrl = ivCover.getTag().toString()
                if (linkUrl.contains("url://")) {
                    val realLink: String = linkUrl.replace("url://", "")
                    SystemUtils.openBrowser(this, realLink)
                }
            }
        }
    }

}