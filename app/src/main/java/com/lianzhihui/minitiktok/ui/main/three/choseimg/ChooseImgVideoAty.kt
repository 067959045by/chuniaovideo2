package com.lianzhihui.minitiktok.ui.main.three.choseimg

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.blankj.utilcode.util.ToastUtils
import com.lianzhihui.minitiktok.base.BaseAty
import com.lianzhihui.minitiktok.base.FragmentParam
import com.lianzhihui.onlyleague.R
import kotlinx.android.synthetic.main.aty_all_top.*
import kotlinx.android.synthetic.main.aty_choose_img_video.*

class ChooseImgVideoAty : BaseAty() {

    var type = "img"

    override fun getLayoutId() = R.layout.aty_choose_img_video

    override fun getFragmentContainerId(): Int = R.id.framlay_content


    fun mainClick(v: View) {

        when (v.id) {
            R.id.tv_back -> {
                finish()
            }

            R.id.tv_left ->{

                if (type=="img"){
                    Log.e("processFragement tag2=",ChooseImgFrg::class.java.toString())

                    var f1 = supportFragmentManager.findFragmentByTag(ChooseImgFrg::class.java.toString())
                    if (f1 is ChooseImgFrg) {
                        var list = f1.returnListCheck()
                        if (list.size == 0) {
                            ToastUtils.showShort("请选择要上传的图片")
                            return
                        }

                        var intent = Intent()
                        intent.putExtra("type", "img_photo")
                        intent.putStringArrayListExtra("data", list)
                        setResult(RESULT_OK, intent)
                        finish()
//
                    }

                }else{

                }

            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg, "#00000000")
        tv_title?.text = "选择分类"
        tv_right?.text = "开拍"
        tv_left?.text = "上传"
        tv_title?.setTextColor(Color.parseColor("#ffffff"))
        tv_right?.setTextColor(Color.parseColor("#ffffff"))
        tv_left?.setTextColor(Color.parseColor("#ffffff"))
        tv_right.visibility = View.VISIBLE
        tv_left.visibility = View.VISIBLE
        relay_back.visibility = View.GONE
        type =intent.getStringExtra("type")
        if (type == "img"){
            var map = HashMap<String, String>()
            map["max_num"] = "1"
            addFragment(ChooseImgFrg::class.java, map)
            v_video.visibility = View.GONE
            v_img.visibility = View.VISIBLE
        }else{
            v_video.visibility = View.VISIBLE
            v_img.visibility = View.GONE
        }
    }
}