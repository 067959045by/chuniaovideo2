package com.lianzhihui.minitiktok.presenter;

import android.content.Context;


import com.blankj.utilcode.util.ToastUtils;
import com.echofeng.common.config.AppConfig;
import com.echofeng.common.net.APIConstant;
import com.echofeng.common.net.HttpManager;
import com.echofeng.common.utils.PreferenceUtils;
import com.lianzhihui.minitiktok.bean.user.LoginResponse;
import com.lianzhihui.minitiktok.view.LoginView;
import com.lianzhihui.minitiktok.model.LoginModelImp;
import com.lianzhihui.minitiktok.model.LoginModelInterface;
import com.lianzhihui.onlyleague.R;
import com.tencent.bugly.proguard.A;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//逻辑实现
public class LoginPresnterImp implements LoginModelInterface {

    private final LoginView loginView;
    private final LoginModelImp loginModelImp;
    private ArrayList<String> list;
    private int listIndex =0;

    public LoginPresnterImp(Context context, LoginView loginView) {
        this.loginView = loginView;
        loginModelImp = new LoginModelImp(context, this);
        List arrList= Arrays.asList(context.getResources().getStringArray(R.array.url_list));
        list= new ArrayList(arrList);

//        list.add("http://api.v1.9you.me");
//        list.add("http://api.v1.9you.site");
//        list.add("http://api.v1.9you.live");
        if(!AppConfig.isFormal2){
            list = new ArrayList<>();
            list.add("http://134.122.170.89:9501");
            list.add("http://134.122.170.28:9501");
        }

        PreferenceUtils.applyString("baseUrl", list.get(listIndex));
        HttpManager.initBaseConfig();
    }

    public void doCheckIp() {
        loginModelImp.doCheckIp();
    }

    public void doLogin(String code, String channel) {
        loginModelImp.doLogin(code, channel);
    }

    public void getPhoneVerifyCode(String phone) {
        loginModelImp.getPhoneVerifyCode(phone);
    }

    public void doPhoneBind(String phone, String code) {
        loginModelImp.doPhoneBind(phone, code);
    }

    @Override
    public void onLoginSuccess(LoginResponse data) {
        loginView.setLoginSuccess(data);
    }

    @Override
    public void onBindPhoneSuccess(Object data) {
        loginView.setBindPhoneSuccess(data);
    }

    @Override
    public void onSuccess(Object data) {
        loginView.setSuccess(data);
    }

    @Override
    public void onFailure(Object data) {
        listIndex++;
        if (listIndex>=list.size()){
            listIndex = 0;
        }
        PreferenceUtils.applyString("baseUrl", list.get(listIndex));
        HttpManager.initBaseConfig();
        doCheckIp();
//      loginView.setFailure(data);

    }


}
