package com.lianzhihui.minitiktok.utils;

public class MyUtils {

    //http://xxx.xx?code=112323&channel=huawei&pcode=jiuyou
    public static  String getCode(String url){
        String[] splitStr = url.split("code=");
        String[] splitStr2 = splitStr[1].split("[&]");
        String code = splitStr2[0];
        return  code;
    }

    public static  String getChannel(String url){
        String[] splitStr = url.split("channel=");
        String[] splitStr2 = splitStr[1].split("[&]");
        String channel = splitStr2[0];
        return  channel;
    }
}
