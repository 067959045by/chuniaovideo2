package com.by.net;


import com.echofeng.common.MyApplication;
import com.echofeng.common.config.AppConfig;
import com.echofeng.common.config.DataManager;
import com.echofeng.common.utils.SystemUtils;
import com.mail.comm.net.ApiListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.common.util.KeyValue;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ApiTool {

    public ApiTool() { }

    RequestParams params;
    String type;

    public Callback.Cancelable getApi(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);
        return x.http().get(params, new DefaultRequestCallBack(apiListener));
    }

    public Callback.Cancelable postApi(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);

        HashMap<String,String> map = new HashMap();
        map.put("device_type", "A");
        map.put("device_no", SystemUtils.getAndroidId(x.app()));
        map.put("version", SystemUtils.getAppVersionName(x.app()));
        map.put("token", DataManager.getAuthorization());
        String singHeader = DataManager.singPostBody( new JSONObject(map));
        params.setHeader("X-TOKEN",singHeader);

        return x.http().post(params, new DefaultRequestCallBack(apiListener));
    }

    public Callback.Cancelable postApi2(RequestParams params, ApiListener apiListener, String type) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(7000);

        HashMap<String,String> map = new HashMap();
        map.put("device_type", "A");
        map.put("device_no", SystemUtils.getAndroidId(x.app()));
        map.put("version", SystemUtils.getAppVersionName(x.app()));
        map.put("token", DataManager.getAuthorization());
        String singHeader = DataManager.singPostBody( new JSONObject(map));
        params.setHeader("X-TOKEN",singHeader);

        initParams(params);
        return x.http().post(params, new DefaultRequestCallBack(apiListener));
    }

    public void initParams( RequestParams params) {
        List<KeyValue> list = params.getBodyParams();
        HashMap map = new HashMap<String, String>();

        for (KeyValue keyValue : list) {
            map.put(keyValue.key,keyValue.value);
        }
        String data = DataManager.singPostBody(new JSONObject(map));
        map.clear();
        map.put("handshake",AppConfig.handshake);
        map.put("data",data);
        params.clearParams();
        params.setBodyContent(new JSONObject(map).toString());

    }



    public Callback.Cancelable postApiTime(RequestParams params, ApiListener apiListener, String type, int time) {
        this.params = params;
        this.type = type;
        params.setConnectTimeout(time);
        return x.http().post(params, new DefaultRequestCallBack(apiListener));
    }

    private class DefaultRequestCallBack implements Callback.ProgressCallback<String> {
        private ApiListener apiListener;

        public DefaultRequestCallBack(ApiListener apiListener) {
            this.apiListener = apiListener;
        }

        @Override
        public void onSuccess(String result) {
            try {
                Map e = parseError(result);
                if (e == null || e.size() == 0) {
                    if (this.apiListener != null) {

                        this.apiListener.onComplete(params, result, type);
                    }
                } else if (this.apiListener != null) {
                    this.apiListener.onError(e, params);
                }
            } catch (Exception var4) {
                if (this.apiListener != null) {
//                    this.apiListener.onExceptionType(var4, params, type);
                }
            }
        }

        public void onError(Throwable ex, boolean isOnCallback) {
            this.apiListener.onExceptionType(ex, params, type);

        }

        public void onCancelled(CancelledException cex) {
            this.apiListener.onCancelled(cex);
        }

        public void onFinished() {

        }

        @Override
        public void onWaiting() {

        }

        @Override
        public void onStarted() {

        }

        @Override
        public void onLoading(long total, long current, boolean isDownloading) {

        }
    }

    public static Map<String, String> parseError(String json) {
        JSONObject jsonObject = null;
        if (json.startsWith("[") && json.endsWith("]")) {
            return null;
        } else {
            try {
                jsonObject = new JSONObject(json);
            } catch (JSONException var4) {
                return null;
            }
            String flag = jsonObject.optString("flag");
            return flag != null && flag.equals("error") ? JSONUtils.parseKeyAndValueToMap(json) : null;
        }
    }
}

