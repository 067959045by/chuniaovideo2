package com.by;

/**
 * Created by pc on 2017/3/3.
 */

public interface MyRefreshAndLoadListen {

     void refreshStart();
     void loadMoreStart();
}
