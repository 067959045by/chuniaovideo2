package com.echofeng.common.config;

public class AppConfig {

    public static String upload_url = "";
    public static String handshake = "";
    public static boolean debug = true;
    public static boolean isFormal2 = false;

    static {

        if (isFormal2){
            handshake = "v20210601";
        }else{
            handshake = "v20210501";
        }
    }

}
